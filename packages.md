# Provenant du projet 19-20

* Minicom
* Git
* Emacs
* Eclipse -> java openjdk-g-sdk
* Wireshark
* Ocaml
* Python et anaconda
* RBase et RStudio

# INFO3 (Informations glanées de Discord) :
## Matières inconnues :
* sudo apt install valgrind
* Digital
* les build-essentials de debian (gcc, objdump,  tout ça);
* Peut être clang, à voir;
* htop

## PW

* apache2
* libapache2-mod-php
* php-cli
* les extensions php php-curl
* php-mbstring
* la commande auxiliaire unicode

## POO
* installation pour AgileLearning

## ALM
* Qemu
* Toute la suite de compilation ARM GCC
* Lustre

## API
* gnuplot (optionnel)

## MN
* hwloc








# INFO4 :

## LTPF

* Télécharger http://www-verimag.imag.fr/~monin/Enseignement/LTPF/robuste/commun/installation/ltpf_2020.emacs et http://www-verimag.imag.fr/~monin/Enseignement/LTPF/robuste/commun/installation/instructions_install_2020.sh. En cas de problème, le fichier log suivant peut être utilisé : http://www-verimag.imag.fr/~monin/Enseignement/LTPF/robuste/commun/installation/opam_install-log.md
* Recopier ltpf_2020.emacs sous ~/.emacs. Il est suggéré de commencer par installer emacs puis son système de packages car c'est le plus rapide.
* Installer également opam, le système de packages pour OCaml (et Coq).
* L'installation de emacs et de opam se fait via le système de packages de la distribution linux, par exemple apt sur Debian ou Ubuntu.
* Lorsque opam est installé, il est facile d'installer OCaml proprement dit puis Coq, ainsi que quelques outils facilitant la programmation comme merlin.
* L'éditeur emacs (avec le package proof-general) est un très bon environnement d'édition de script Coq ; on peut aussi utiliser coqide à cet effet, dont l'installation dépend de davantages de packages système ou opam.

## Technologie des Réseaux

Voir dans dossier install

## IHM

(optionnel) https://ant.apache.org/

## PS

RStudio : https://www.rstudio.com/products/rstudio/download/#download

## Architecture des systèmes

Eclipse

# INFO5 (Informations glanées de Discord) :
* Rstudio
* minicom
* xterm

## ECOM
* JHipster

