# Tableau de bord

*17 january :*

Started learning about nix. Installed the hello word package through nixpkgs.
Start looking at the website of nixos for a tutorial [link of nixos website](https://nixos.org/guides/dev-environment.html).
Start learning about nix flake.

*24 january :*

Most team members have COVID. No real work could be done.
Efforts to understand nix flake continued.

*31 january :*

Some explanations were provided by Mr Olivier Richard.
Following this, nix flake was put aside in favor of nix pill.
Started reading Nix Pills. All team members red at list up to chapter 6.
The tutorial is available here: [Nix Pill tutorial](https://nixos.org/guides/nix-pills/index.html)

*7 february :*

Nix Pill tutorial has been advanced up to the 9th chapter.
Start making our own build, simultaneously learning the Nix Expression Language used all along the tutorial.
Our first C program was created using nix. It use a C programm and a builder.

*14 february :*

Preparation of the mid-term presentation with O. Richard, it was decided to start the package management and stop learning more about nix derivation.

*23 and 24 february :*

Start learning more about nix package and reading the documentation about it made by the previous group.

*26 february :*

The first derivation with minicom called RX was created, when the user call the nix-shell with RX as an argument, it install minicom and launch a nix shell where the minicom command are perform.

*28 february*

Mid term presentation.
Found the last groups NUR repository and started studying it.

*01 march*

Merging with the old work to get package, start learning more on nix-flakes. Not able to nix build with the old work from INFO4-19 of polytech, it's only possible to execute in a nix-shell.

*07 march*

Further research on Nix flakes and started right our own *flake documentation*

*08 march*

Little meeting with Mr O. Richard to talk about the expected result and about nix flakes in our use.

*12 & 13 march*

Working to finish the differents nix package in nix shell. This need to be finished and a meeting group to talk about.
Now it's time to focus on nix flakes to be able to weigh the pros and cons for each method (Nix shell and Nix flakes).

*14 march*

Start working in a bash script assuming nix is already installed wich install different nix package to use them as normal programm. Continue working on nix flakes to see differences.

*15 march*

The script is able to read the argument and many classes are done. Nix flakes are ongoing.

*20 march*

The script is done, need to meet to talk about it.

*21 march*

The script was finished, Continue working on nix flakes with other website

*22 march*

Meeting with O. Richard, new ressource unlocked thanks to him. Forked the project to work on nix flakes with nix env and a git repos

*28 march*

Finished work on the new repos, new classes added, all seems to be done, finished with making a pipeline to create a release. Now, people can use our flakes with the url of the repos.

*29 march*

Working on the final report

